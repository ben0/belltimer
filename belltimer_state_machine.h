#ifndef __INCLUDE_GUARD_BELLTIMER_STATE_MACHINE_H
#define __INCLUDE_GUARD_BELLTIMER_STATE_MACHINE_H

#include <stdint.h>

namespace belltimer {

// Bell striking behavior
constexpr uint8_t kNumBellStrikes = 3;
constexpr uint16_t kBellStrikeIntervalMsec = 5000;

// Digit blinking behavior (when setting the minutes or seconds)
constexpr uint16_t kBlinkRateMsec = 500;

// Repeat rate when long-pressing the Up or Down button
constexpr uint16_t kButtonRepeatInitialDelayMsec = 1000;
constexpr uint16_t kButtonRepeatDelayMsec = 400;

/* Return type of `millis` (milliseconds timestamp).  See:
 * https://www.arduino.cc/reference/en/language/functions/time/millis
 *
 * If this library depended on `Arduino.h`, I would define this type
 * as `decltype(millis())`; but let's not bring in a heavyweight
 * dependency solely for that reason.  The caller can check this
 * type equality via `static_assert` if needed.
 */
using MillisTimestamp = unsigned long;

enum struct BellTimerState {
    /*
     * The clock display shows a solid 00:00.  The user must set the clock
     * display to a nonzero amount of time before resuming the timer.
     */
    ZERO,

    /*
     * The user may set the minutes display to anywhere between 00 and 99 via
     * the Up and Down buttons.
     *
     * A short press increments the number by 1; a long press first rounds the
     * number (up or down, according to the button) to the nearest multiple of
     * 5, then increments the number by 5 repeatedly for the duration the
     * button is held.
     *
     * When no buttons are being pressed, the minutes digits are blinking (and
     * the seconds digits remain solid).  While buttons are being pressed (thus
     * the minutes display is changing), all digits are solid.  The timer of
     * the blink cycle restarts when the button is released, so that the
     * minutes display does not disappear immediately upon button release.
     */
    SET_MINS,

    /*
     * Similar to `SET_MINS`, except:
     *
     * - The seconds display is the one that changes (and blinks), not the
     *   minutes display.
     * - The range of values for the seconds display is 00 to 59, not 00 to 99.
     */
    SET_SECS,

    /*
     * Paused state.  The amount of remaining time is displayed (solid),
     * rounded up to the nearest second.  (Rounding up because I think users
     * expect a display of 0 to mean time is up, and might get confused
     * otherwise.)
     */
    PAUSED,

    /*
     * Counting down state.  The only button that does anything in this state
     * is Pause/Resume, which returns to the `PAUSED` state.  If the timer
     * reaches zero, the state is changed to `DING`.
     */
    COUNTDOWN,

    /*
     * Bell-striking state.  If there are no button presses, the striker is
     * pulsed three times and then the state changes to `ZERO`.
     *
     * If the Pause/Resume button is pressed, remaining bell strikes are
     * cancelled and the state changes to `ZERO`.
     *
     * If the Set button is pressed, the remaining bell strikes are curtailed
     * and the state changes to `SET_MINS` with the clock display initially
     * showing 00:00.
     */
    DING,
};

enum struct ButtonInput {
    NONE,
    SET,
    PAUSE_RESUME,
    UP,
    DOWN,
};

/*
 * State machine that determines the bell timer's behavior.
 *
 * The "state" maintained by this state machine includes:
 *
 * - The current `BellTimerState`
 * - The current timestamp
 * - The current user input (`ButtonInput`)
 * - The current minutes and seconds for the user-facing clock: the minutes and
 *   seconds are remembered even when the clock is paused, and even when they
 *   are not being shown on the physical display.
 *
 * In addition, pieces of a few past states are remembered; namely, the
 * previous state and the state at which the current user input started (i.e.,
 * when they started pressing the button that they are currently holding).
 *
 * For convenience, a few other member variables hold pre-computed values of
 * behavior-relevant data derived from the above.  These variables take care of
 * blinking the display at the appropriate times and determining when a bell
 * strike should be started, so that the driver loop doesn't have to compute
 * those on its own.  (The driver loop does have to know how to drive the
 * motor, though; this state machine only tells it when to start a strike, not
 * how to implement the strike mechanically.)
 */
class StateMachine {
   public:
    StateMachine();

    /*
     * Advances the state machine based on the information that user has input
     * `input` at time `ts_msec`.
     */
    void step(ButtonInput input, MillisTimestamp ts_msec);

    inline BellTimerState getState() const { return state_; }
    inline uint8_t getMins() const { return mins_display_; }
    inline uint8_t getSecs() const { return secs_display_; }
    inline bool minsVisible() const { return !hide_mins_; }
    inline bool secsVisible() const { return !hide_secs_; }
    inline bool strikeStartsNow() const { return strike_starts_now_; }

   private:
    BellTimerState state_;
    // State of the machine at the previous step
    BellTimerState prev_state_;
    MillisTimestamp prev_ts_msec_;

    ButtonInput prev_input_;
    // Time at which the current run of identical consecutive input values
    // started.  That is, when the user starting pressing the current button.
    MillisTimestamp input_hold_start_ts_msec_;

    // Numeric values that would be displayed as the minutes (first two digits)
    // and seconds (last two digits), if not for blinking and/or the "ding"
    // text.
    uint8_t mins_display_;
    uint8_t secs_display_;
    // Indicator of whether the display digits for minutes (resp., seconds)
    // should be hidden, due to being in the middle of a blink.
    bool hide_mins_;
    bool hide_secs_;

    // Time at which the current countdown would end, assuming no
    // interruptions.
    MillisTimestamp countdown_end_ts_msec_;

    // Time at which the machine most recently entered the `DING` state.
    MillisTimestamp ding_start_ts_msec_;
    // Indicator of whether the logic controlling the striker should start a
    // strike during this state machine step.  Will only be set to `true` for
    // one state machine step per strike.  This state machine does not give a
    // prescription of how to strike the bell in terms of a physical motor (for
    // example, when to turn off the solenoid), that is up to the caller.
    bool strike_starts_now_;


    void stepFromZero(ButtonInput input, MillisTimestamp ts_msec,
                      bool input_is_new, uint32_t input_hold_duration_msec,
                      BellTimerState* next_state);

    void stepFromSetMins(ButtonInput input, MillisTimestamp ts_msec,
                         bool input_is_new, uint32_t input_hold_duration_msec,
                         BellTimerState* next_state);

    void stepFromSetSecs(ButtonInput input, MillisTimestamp ts_msec,
                         bool input_is_new, uint32_t input_hold_duration_msec,
                         BellTimerState* next_state);

    void stepFromPaused(ButtonInput input, MillisTimestamp ts_msec,
                        bool input_is_new, uint32_t input_hold_duration_msec,
                        BellTimerState* next_state);

    void stepFromCountdown(ButtonInput input, MillisTimestamp ts_msec,
                           bool input_is_new,
                           uint32_t input_hold_duration_msec,
                           BellTimerState* next_state);

    void stepFromDing(ButtonInput input, MillisTimestamp ts_msec,
                      bool input_is_new, uint32_t input_hold_duration_msec,
                      BellTimerState* next_state);
};

}  // namespace belltimer

#endif  // __INCLUDE_GUARD_BELLTIMER_STATE_MACHINE_H
