#include "belltimer_state_machine.h"

#include <stdint.h>

namespace belltimer {

namespace {

using BI = ButtonInput;
using BTS = BellTimerState;

// Modular addition and subtraction operators that are overflow-safe for
// unsigned integral types `U`.  Returns a value between 0 and `m - 1`,
// inclusive.  From [1], section 39.1.1.
//
// [1] Arndt, Joerg. _Matters Computational._ Available at:
//     https://www.jjj.de/fxt/#fxtbook
template <typename U>
inline U unsignedSubMod(U a, U b, U m) {
    if (a >= b) {
        return a - b;
    } else {
        return m - b + a;
    }
}

template <typename U>
inline U unsignedAddMod(U a, U b, U m) {
    a = a % m;
    b = b % m;
    if (b == 0) {
        return a;
    }
    return unsignedSubMod(a, static_cast<U>(m - b), m);
}

// Helper function to reduce the amount of if-else biolerplate in the business
// logic
template <typename U>
inline U unsignedAddOrSubMod(bool add, U a, U b, U m) {
    return (add ? unsignedAddMod(a, b, m) : unsignedSubMod(a, b, m));
}

// If `inc` is true (resp., false), then returns the smallest positive `d` such
// that `a + d` (resp., `a - d`) is a multiple of `m`.
//
// Requires `m > 0` and `a >= 0`.  Recommend having `U` be an unsigned type.
template <typename U>
U smallestNonzeroDeltaToMultiple(bool inc, U a, U m) {
    a = a % m;
    if (a == 0) {
        return m;
    }
    else if (inc) {
        return static_cast<U>(m - a);
    } else {
        return a;
    }
}

inline constexpr uint8_t operator "" _ui8(unsigned long long x) noexcept {
    return static_cast<uint8_t>(x);
}
inline constexpr uint16_t operator "" _ui16(unsigned long long x) noexcept {
    return static_cast<uint16_t>(x);
}
inline constexpr uint32_t operator "" _ui32(unsigned long long x) noexcept {
    return static_cast<uint32_t>(x);
}

/* Returns the smallest `T` less than or equal to `duration` that is equal to
 *
 *     initial_delay + c * repeat_delay
 *
 * for some nonnegative integer `c`.  If `duration < initial_delay`, instead
 * returns 0.
 *
 * All arguments must be nonnegative, and `repeat_delay` must be positive.
 * Recommend having `T` and `U` be unsigned types.
 */
template <typename T, typename U>
inline T prevRepeatTime(T duration, U initial_delay, U repeat_delay) {
    if (duration < initial_delay) {
        return 0;
    }
    return duration - ((duration - initial_delay) % repeat_delay);
}

/*
 * Updates `*display_val` based on the given information about current state
 * and inputs.  This is the workhorse function for the `SET_MINS` and
 * `SET_SECS` states.
 *
 * In summary, the behavior is:
 * * At beginning of Up (resp., Down) button press, increment (resp.,
 *   decrement) the display value by 1.
 * * If the user is holding Up (resp., Down), first increment (resp.,
 *   decrement) to the nearest multiple of 5 and then increment (resp.,
 *   decrement) by 5 every `kButtonRepeatDelayMsec` milliseconds.
 * * When the display value reaches `wrap_at`, it wraps back to zero.
 *   Similarly, when decrementing from zero, the result wraps back to
 *   `wrap_at`.
 *
 * Parameters:
 * `input`: The button being pressed (or `NONE` if no button is being
 *     pressed).  Must be either `UP` or `DOWN`, otherwise the behavior of this
 *     function is undefined.
 * `input_is_new`: True if the input's value is different from what it was at
 *     the previous state machine step.  In other words, true if the user is
 *     just starting to press the button, as opposed to in the middle of
 *     holding the button.
 * `input_hold_duration_msec`: Number of milliseconds the input has been at
 *     its current value (i.e. number of milliseconds the user has been holding
 *     the button).
 * `prev_input_hold_duration_msec`: Like `input_hold_duration_msec`, but
 *     measured at the *previous* state machine step.  If `input_is_new` is
 *     true, this value is not used (and the above definition would not make
 *     sense).
 * `wrap_at`: Modulus at which the display value wraps to zero.  For example,
 *     if `wrap_at = 60`, then the range of possible display values is 0 to 59.
 * `display_val`: Input/output argument.  Must be non-`nullptr`.  At call time,
 *     `*display_val` is the current value shown in the display (or would be
 *     shown, if not for blinking).  At return time, `*display_val` is the
 *     updated value.
 */
void handleIncDec(ButtonInput input, bool input_is_new,
                  uint16_t input_hold_duration_msec,
                  uint16_t prev_input_hold_duration_msec,
                  uint8_t wrap_at, uint8_t* display_val) {
    if (input_is_new) {
        *display_val = unsignedAddOrSubMod(
                input == BI::UP, *display_val, 1_ui8, wrap_at);
    } else {
        // Determine if this is a long press, and if so, whether
        // we are due for another repeat
        if (prev_input_hold_duration_msec < prevRepeatTime(
                    input_hold_duration_msec, kButtonRepeatInitialDelayMsec,
                    kButtonRepeatDelayMsec)) {
            // If already at a multiple of 5, increase (resp.,
            // decrease) by 5.  Otherwise, round up (resp., down) to
            // nearest multiple of 5.
            *display_val = unsignedAddOrSubMod(
                    input == BI::UP,
                    *display_val,
                    smallestNonzeroDeltaToMultiple(
                        input == BI::UP, *display_val, 5_ui8),
                    wrap_at);
        }
    }
}

inline uint32_t minsSecsToMsec(uint8_t mins, uint8_t secs) {
    return 1000_ui32 * (60 * mins + secs);
}

inline void truncateMsecsToMinsSecs(uint32_t msecs, uint8_t* mins,
                                    uint8_t* secs) {
    // Division with rounding up
    const auto full_secs = msecs / 1000 + (msecs % 1000 != 0);
    // Truncate to a value less than 100 minutes, since values greater than 100
    // minutes don't fit on the display (and are not supported by this machine)
    const auto truncated_secs = full_secs % (100 * 60);
    *secs = truncated_secs % 60;
    *mins = truncated_secs / 60;
}

}  // namespace

StateMachine::StateMachine() :
    state_(BTS::ZERO), prev_state_(BTS::ZERO), prev_ts_msec_(0),
    prev_input_(BI::NONE), input_hold_start_ts_msec_(0),
    mins_display_(0), secs_display_(0), hide_mins_(false), hide_secs_(false),
    countdown_end_ts_msec_(0), ding_start_ts_msec_(0),
    strike_starts_now_(false) {}

void StateMachine::step(const ButtonInput input,
                        const MillisTimestamp ts_msec) {
    hide_mins_ = false;
    hide_secs_ = false;
    strike_starts_now_ = false;

    const bool input_is_new = (input != prev_input_);
    if (input_is_new) {
        input_hold_start_ts_msec_ = ts_msec;
    }
    const auto input_hold_duration_msec = ts_msec - input_hold_start_ts_msec_;

    BTS next_state;
    if (state_ == BTS::ZERO) {
        stepFromZero(input, ts_msec, input_is_new, input_hold_duration_msec,
                     &next_state);
    } else if (state_ == BTS::SET_MINS) {
        stepFromSetMins(input, ts_msec, input_is_new, input_hold_duration_msec,
                        &next_state);
    } else if (state_ == BTS::SET_SECS) {
        stepFromSetSecs(input, ts_msec, input_is_new, input_hold_duration_msec,
                        &next_state);
    } else if (state_ == BTS::PAUSED) {
        stepFromPaused(input, ts_msec, input_is_new, input_hold_duration_msec,
                       &next_state);
    } else if (state_ == BTS::COUNTDOWN) {
        stepFromCountdown(input, ts_msec, input_is_new,
                          input_hold_duration_msec, &next_state);
    } else if (state_ == BTS::DING) {
        stepFromDing(input, ts_msec, input_is_new, input_hold_duration_msec,
                     &next_state);
    } else {
        // How did we get here...
        next_state = BTS::ZERO;
    }

    prev_state_ = state_;
    state_ = next_state;
    prev_input_ = input;
    prev_ts_msec_ = ts_msec;
}

void StateMachine::stepFromZero(
        const ButtonInput input, const MillisTimestamp ts_msec,
        const bool input_is_new, const uint32_t input_hold_duration_msec,
        BellTimerState* next_state) {
    if (!input_is_new) {
        // Do nothing; all the cases below assume `input_is_new`
        *next_state = state_;
    } else if (input == BI::SET) {
        *next_state = BTS::SET_MINS;
        mins_display_ = 0;
        secs_display_ = 0;
    } else {
        *next_state = BTS::ZERO;
    }
}

void StateMachine::stepFromSetMins(
        const ButtonInput input, const MillisTimestamp ts_msec,
        const bool input_is_new, const uint32_t input_hold_duration_msec,
        BellTimerState* next_state) {
    if (input == BI::UP || input == BI::DOWN) {
        const auto prev_input_hold_duration_msec = (input_is_new ?
                0  /* <-- arbitrary choice, has no meaning */ :
                prev_ts_msec_ - input_hold_start_ts_msec_);
        handleIncDec(input, input_is_new, input_hold_duration_msec,
                     prev_input_hold_duration_msec, 100, &mins_display_);
    } else if (input == BI::NONE) {
        *next_state = BTS::SET_MINS;
        // Blinking only happens while no buttons are being pressed.
        // Blinks start with the visible state (because starting with the
        // invisible state could be confusing to the user).
        hide_mins_ = ((input_hold_duration_msec / kBlinkRateMsec) % 2 == 1);
    } else if (!input_is_new) {
        // Do nothing; all the cases below assume `input_is_new`
        *next_state = state_;
    } else if (input == BI::PAUSE_RESUME) {
        if (mins_display_ == 0 && secs_display_ == 0) {
            *next_state = BTS::SET_MINS;
        } else {
            *next_state = BTS::COUNTDOWN;
            countdown_end_ts_msec_ = ts_msec + minsSecsToMsec(mins_display_,
                                                              secs_display_);
        }
    } else if (input == BI::SET) {
        *next_state = BTS::SET_SECS;
    } else {
        // How did we get here...
        *next_state = BTS::SET_MINS;
    }
}

void StateMachine::stepFromSetSecs(
        const ButtonInput input, const MillisTimestamp ts_msec,
        const bool input_is_new, const uint32_t input_hold_duration_msec,
        BellTimerState* next_state) {
    if (input == BI::UP || input == BI::DOWN) {
        const auto prev_input_hold_duration_msec = (input_is_new ?
                0  /* <-- arbitrary choice, has no meaning */ :
                prev_ts_msec_ - input_hold_start_ts_msec_);
        handleIncDec(input, input_is_new, input_hold_duration_msec,
                     prev_input_hold_duration_msec, 60, &secs_display_);
    } else if (input == BI::NONE) {
        *next_state = BTS::SET_SECS;
        // Blinking only happens while no buttons are being pressed.
        // Blinks start with the visible state (because starting with the
        // invisible state could be confusing to the user).
        hide_secs_ = ((input_hold_duration_msec / kBlinkRateMsec) % 2 == 1);
    } else if (!input_is_new) {
        // Do nothing; all the cases below assume `input_is_new`
        *next_state = state_;
    } else if (input == BI::PAUSE_RESUME) {
        if (mins_display_ == 0 && secs_display_ == 0) {
            *next_state = BTS::SET_SECS;
        } else {
            *next_state = BTS::COUNTDOWN;
            countdown_end_ts_msec_ = ts_msec + minsSecsToMsec(mins_display_,
                                                              secs_display_);
        }
    } else if (input == BI::SET) {
        if (mins_display_ == 0 && secs_display_ == 0) {
            *next_state = BTS::ZERO;
        } else {
            *next_state = BTS::PAUSED;
            countdown_end_ts_msec_ = ts_msec + minsSecsToMsec(mins_display_,
                                                              secs_display_);
        }
    } else {
        // How did we get here...
        *next_state = BTS::SET_SECS;
    }
}

void StateMachine::stepFromPaused(
        const ButtonInput input, const MillisTimestamp ts_msec,
        const bool input_is_new, const uint32_t input_hold_duration_msec,
        BellTimerState* next_state) {
    if (!input_is_new) {
        // Do nothing; all the cases below assume `input_is_new`
        *next_state = state_;
    } else if (input == BI::SET) {
        *next_state = BTS::SET_MINS;
    } else if (input == BI::PAUSE_RESUME) {
        *next_state = BTS::COUNTDOWN;
        countdown_end_ts_msec_ = ts_msec + minsSecsToMsec(mins_display_,
                                                     secs_display_);
    } else {
        *next_state = BTS::PAUSED;
    }
}

void StateMachine::stepFromCountdown(
        const ButtonInput input, const MillisTimestamp ts_msec,
        const bool input_is_new, const uint32_t input_hold_duration_msec,
        BellTimerState* next_state) {
    const auto remaining_msec = countdown_end_ts_msec_ - ts_msec;
    truncateMsecsToMinsSecs(remaining_msec, &mins_display_, &secs_display_);
    if (ts_msec > countdown_end_ts_msec_) {
        // If the user presses Set or Pause/Resume during the exact same
        // state machine step as time runs out, then this code path will
        // miss that and still move to the `DING` state for one step.  This
        // is ok because
        // (1) This will be extremely rare
        // (2) The user won't be able to tell the difference between this
        //     and the case where they pressed the button one step after
        //     time ran out
        // (3) The machine will move out of the `DING` state on the next
        //     step (assuming the button is still being held down).
        *next_state = BTS::DING;
        mins_display_ = 0;
        secs_display_ = 0;
        ding_start_ts_msec_ = ts_msec;
    } else if (!input_is_new) {
        // Do nothing; all the cases below assume `input_is_new`
        *next_state = state_;
    } else if (input == BI::PAUSE_RESUME) {
        *next_state = (mins_display_ == 0 && secs_display_ == 0 ?
                      BTS::ZERO : BTS::PAUSED);
    } else {
        *next_state = BTS::COUNTDOWN;
    }
}

void StateMachine::stepFromDing(
        const ButtonInput input, const MillisTimestamp ts_msec,
        const bool input_is_new, const uint32_t input_hold_duration_msec,
        BellTimerState* next_state) {
    // No need to worry about `input_is_new` for this case -- the only
    // buttons that do anything are Set and Pause/Resume; and neither of
    // those buttons will be held down when the machine enters the `DING`
    // state.
    if (input == BI::PAUSE_RESUME) {
        *next_state = BTS::ZERO;
    } else if (input == BI::SET) {
        *next_state = BTS::SET_MINS;
        mins_display_ = 0;
        secs_display_ = 0;
    } else if (ts_msec >= (ding_start_ts_msec_ +
                           kNumBellStrikes * kBellStrikeIntervalMsec)) {
        *next_state = BTS::ZERO;
    } else {
        if (prev_state_ != BTS::DING ||
            prev_ts_msec_ <  /* Note: strictly less; this way
                                we do not add an extra strike */
            ding_start_ts_msec_ + prevRepeatTime(
                    static_cast<uint16_t>(ts_msec - ding_start_ts_msec_),
                    0_ui16, kBellStrikeIntervalMsec)) {
            strike_starts_now_ = true;
        }
        *next_state = BTS::DING;
    }
}

}  // namespace belltimer
