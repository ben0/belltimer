# Bell Timer

A meditation timer that strikes a bell when the time is up.

![Perfboard prototype, boxed](prototype_boxed.jpg)

![Breadboard prototype](prototype_breadboard.jpg)

Also check out these videos of the
[breadboard prototype](https://drive.google.com/file/u/1/d/1JJ6JJ8dw-oKcwaKVFNziGij31Zeo6ImT/view?usp=sharing)
and a more polished
[perfboard prototype](https://drive.google.com/file/d/1bEydlcHvis2DRt3GGvlCLbKKdxZzJSEe/view?usp=sharing)
which show the bell striking in action.


## Components

* [Adafruit Metro Mini 328](https://www.adafruit.com/product/2590), or can
  substitute an Arduino.  We use the hardware I2C bus and the internal pull-up
  resistors, though you could work around these for a board that doesn't have
  them.
* 12V DC push/pull solenoid to strike the bell, e.g.
  [this](https://www.adafruit.com/product/412) one
* 4-digit, 7-segment display with
  [Adafruit driver board](https://www.adafruit.com/product/1002) -- very
  convenient, fits like a glove; we use their
  [C++ libraries](https://github.com/adafruit/Adafruit_LED_Backpack).
* Some form of 12V DC input capable of giving enough burst current to drive the solenoid, e.g.:
  + [DC barrel jack](https://www.adafruit.com/product/373) + [wall adapter](https://www.adafruit.com/product/798)
  + 8×AA holder + 1000μF, ≥ 12V electrolytic capacitor + 1N4001 diode for isolation + 2.2kΩ resistor for bleeder
* 4 button switches -- the button functions are Set, Pause/Resume, Up, and Down
* 1 power transistor to control the solenoid, e.g.
  [TIP120](https://www.adafruit.com/product/976)
* 1 diode for flyback protection, e.g. [1N4001](https://www.adafruit.com/product/755)
* 1 resistor, 1kΩ
* Breadboard (or perfboard, stripboard, etc.) and jumper wires

## Circuit

![Circuit schematic](circuit.svg)

### Power supply

To support the burst current when running with a battery, can use a 1000μF,
≥ 12V capacitor as follows:

![Power supply schematic](power_supply.svg)

## Firmware build

### Dependencies

You'll need these C++ libraries in your include path:

* [`Adafruit_LED_Backpack`](https://github.com/adafruit/Adafruit_LED_Backpack/)
* [`Adafruit_GFX`](https://github.com/adafruit/Adafruit-GFX-Library)
* [`Adafruit_BusIO`](https://github.com/adafruit/Adafruit_BusIO)

### Option 1: Use the Arduino IDE

I've never done this before, so you're on your own :-)

### Option 2: Use the Makefile

If you are using an Arduino Uno–compatible board, simply run

    make

to compile the program, and then

    make upload

to flash your controller.  If you are re-building, precede both of the above
steps with

    make clean

There are several build options at the top of the Makefile that can be set
from the command line, e.g.

    make PIN_BUTTON_SET=2 \
        PIN_BUTTON_PAUSE_RESUME=5 \
        PIN_BUTTON_UP=8 \
        PIN_BUTTON_DOWN=11 \
        PIN_SOLENOID_CONTROL=3 \
        STRIKE_DURATION_MSEC=50

The heavy lifting is done by
[Arduino-Makefile](https://github.com/sudar/Arduino-Makefile), so if you are
using a non-Uno board, you will need to supply the build arguments `BOARD_TAG`
and `BOARD_SUB` as described in the Arduino-Makefile documentation.  For
example:

    make BOARD_TAG=atmegang BOARD_SUB=atmega168
    make upload

Note that Arduino-Makefile depends on (the Arduino core libraries and)
pySerial.
