#ifndef __INCLUDE_GUARD_DEBOUNCE_H
#define __INCLUDE_GUARD_DEBOUNCE_H

namespace debounce {

constexpr uint16_t kMinReadsDefault = 50;

/*
 * Bookkeeping class for tracking enough of a switch's state history to
 * determine whether the switch is at steady state.
 *
 * The parameter `delay` is assumed to be an upper bound on the amount of time
 * the switch's mechanical state must be held constant before the digital input
 * controlled by the switch will reach steady state.  This is the "debouncing"
 * delay. This class currently does not allow for different delays for on ->
 * off versus off -> on.
 *
 * The switch is considered to be at steady state when both of the following
 * hold:
 *
 * - The switch's momentary state has been the same for at least `delay` amount
 *   of time.
 * - The switch's momentary state has been the same for at least `min_reads`
 *   reads.
 *
 * The units (and even the types) of the timestamps (`TS`) and the delay
 * duration (`D`) are up to the caller, as long as the necessary `+` and `<`
 * operators are defined such that `timestamp1 + delay < timestamp2` has the
 * correct meaning.
 */
template <typename MS, typename TS, typename D>
class SwitchState {
   public:
    using MomentaryState = MS;
    using Timestamp = TS;
    using Duration = D;

    SwitchState(D delay, MS initial_state, TS initial_ts, 
                uint16_t min_reads = kMinReadsDefault) :
        delay_(delay),
        state_(initial_state),
        most_recent_ts_(initial_ts),
        consistent_since_ts_(initial_ts),
        num_consistent_reads_(1),
        min_reads_(min_reads) {}

    /*
     * Incorporate a new reading that the switch's momentary state at time `ts`
     * is `state`.
     */
    void logRead(MS state, TS ts) {
        if (state == state_) {
            // We only care whether `num_consistent_reads_` is greater or less
            // than `min_reads_`.  So cap its value so that it fits inside a
            // `uint16_t`.
            if (num_consistent_reads_ < min_reads_) {
                ++num_consistent_reads_;
            }
        } else {
            consistent_since_ts_ = ts;
            num_consistent_reads_ = 1;
        }
        state_ = state;
        most_recent_ts_ = ts;
    }

    // Returns the most recently logged state.
    inline MS curState() const {
        return state_;
    }

    // Returns `true` if the switch is at steady state (as of the time of its
    // most recently logged state).
    inline bool isSteady() const {
        return (num_consistent_reads_ >= min_reads_ &&
                most_recent_ts_ >= consistent_since_ts_ + delay_);
    }

   private:
    const D delay_;
    MS state_;
    TS most_recent_ts_;
    TS consistent_since_ts_;
    uint16_t num_consistent_reads_;
    const uint16_t min_reads_;
};

// Factory function that infers the template parameters from argument types
template <typename MS, typename TS, typename D>
SwitchState<MS, TS, D> makeSwitchState(D delay, MS initial_state,
                                       TS initial_ts) {
    return SwitchState<MS, TS, D>(delay, initial_state, initial_ts);
}

}  // namespace debounce

#endif  // __INCLUDE_GUARD_DEBOUNCE_H
