/*
 * Timer that strikes a bell when the time is up.
 *
 * For the circuit diagram, see
 * https://github.com/bzinberg/belltimer
 */

#include <Arduino.h>

#include <Adafruit_GFX.h>
#include <Adafruit_LEDBackpack.h>

#include "belltimer_state_machine.h"
#include "debounce.h"

namespace {

// Reimplementation of `std::size`
template <typename T, size_t N>
constexpr size_t static_length(T (&a)[N]) { return N; }

// Reimplementation of `std::is_same`
template <typename T, typename U>
struct is_same { static constexpr bool value = false; };
template <typename T>
struct is_same<T, T> { static constexpr bool value = true; };

template <typename T, size_t N>
constexpr bool all_distinct(T const (&a)[N]) {
    // Thank you C++14 for allowing non-constant objects to be created inside a
    // `constexpr` function body
    for (size_t i = 0; i < N; ++i) {
        for (size_t j = i + 1; j < N; ++j) {
            if (a[i] == a[j]) {
                return false;
            }
        }
    }
    return true;
}

using ::belltimer::BellTimerState;
using ::belltimer::ButtonInput;
using ::belltimer::MillisTimestamp;
static_assert(is_same<MillisTimestamp, decltype(millis())>::value);

using BI = ButtonInput;
using BTS = BellTimerState;

constexpr uint8_t kDisplayI2CAddr = DISPLAY_I2C_ADDR;

constexpr uint8_t kPinButtonSet          = PIN_BUTTON_SET;
constexpr uint8_t kPinButtonPauseResume  = PIN_BUTTON_PAUSE_RESUME;
constexpr uint8_t kPinButtonUp           = PIN_BUTTON_UP;
constexpr uint8_t kPinButtonDown         = PIN_BUTTON_DOWN;

constexpr uint8_t kPinSolenoidControl    = PIN_SOLENOID_CONTROL;
// Number of milliseconds the solenoid should be turned on for each individual
// strike of the bell.  (This is not the delay between consecutive strikes, it
// is the small amount of time between when the solenoid pushes out and when it
// pulls back in.)
constexpr uint16_t kStrikeDurationMsec = STRIKE_DURATION_MSEC;
MillisTimestamp solenoid_push_expiry_ts = 0;

static_assert(all_distinct({
        kPinButtonSet,
        kPinButtonPauseResume,
        kPinButtonUp,
        kPinButtonDown,
        kPinSolenoidControl,
}));

// Canonical order for the non-`NONE` `ButtonInput`s.  Note,
// `kOrderedButtonPins` and `ordered_switch_states` must match this order.
// (This contract is a poor man's `std::map`.)
constexpr ButtonInput kOrderedButtonInputs[] = {BI::SET, BI::PAUSE_RESUME,
                                                BI::UP, BI::DOWN};

// Array containing the digital input pins for the buttons, **in the same order
// as their corresponding `ButtonInput`s in `kOrderedButtonInputs`.**  Thus,
// has the same number of elements as `kOrderedButtonInputs`.
constexpr uint8_t kOrderedButtonPins[] = {kPinButtonSet, kPinButtonPauseResume,
                                          kPinButtonUp, kPinButtonDown};
static_assert(static_length(kOrderedButtonPins) ==
              static_length(kOrderedButtonInputs));

constexpr uint16_t kButtonDebounceMsec = BUTTON_DEBOUNCE_MSEC;

auto s_set = debounce::makeSwitchState(kButtonDebounceMsec, HIGH, millis());
auto s_pause_resume = debounce::makeSwitchState(kButtonDebounceMsec, HIGH, millis());
auto s_up = debounce::makeSwitchState(kButtonDebounceMsec, HIGH, millis());
auto s_down = debounce::makeSwitchState(kButtonDebounceMsec, HIGH, millis());

// Array containing the button switch states, **in the same order as their
// corresponding `ButtonInput`s in `kOrderedButtonInputs`.**  Thus, has the
// same number of elements as `kOrderedButtonInputs`.
decltype(s_set)* const ordered_switch_states[] = {
    &s_set, &s_pause_resume, &s_up, &s_down};
static_assert(static_length(ordered_switch_states) ==
              static_length(kOrderedButtonInputs));

/* 
 * Returns the currently pressed button, PROVIDED THAT all of the following
 * conditions are met:
 *
 * - All button inputs are at steady state
 * - Exactly one button input is pressed
 *
 * If not all of the above conditions are met, returns `ButtonInput::NONE`.
 */
ButtonInput getOnlyInput() {
    ButtonInput ret = BI::NONE;
    for (size_t i = 0; i < static_length(ordered_switch_states); ++i) {
        const auto* s = ordered_switch_states[i];
        if (!s->isSteady()) {
            return BI::NONE;
        }
        if (s->curState() == LOW) {
            if (ret != BI::NONE) {
                // More than one input is on
                return BI::NONE;
            }
            ret = kOrderedButtonInputs[i];
        }
    }
    return ret;
}

auto display = Adafruit_7segment();
auto sm = belltimer::StateMachine();

/* Write the number of minutes to the first two digits of the display. */
void writeMins(uint8_t mins) {
    if (mins >= 10) {
        display.writeDigitNum(0, mins / 10);
    } else {
        display.writeDigitRaw(0, 0);
    }
    display.writeDigitNum(1, mins % 10);
}

/* Write the number of seconds to the last two digits of the display. */
void writeSecs(uint8_t secs) {
    display.writeDigitNum(3, secs / 10);
    display.writeDigitNum(4, secs % 10);
}

/* Make the first two digits of the display blank. */
void writeMinsBlank() {
    display.writeDigitRaw(0, 0);
    display.writeDigitRaw(1, 0);
}

/* Make the last two digits of the display blank. */
void writeSecsBlank() {
    display.writeDigitRaw(3, 0);
    display.writeDigitRaw(4, 0);
}

/* Write the word "ding" to the display. */
void writeDing() {
    // For the mapping from bit mask to physical location on the 7-segment
    // digit (+ decimal point), see:
    // https://forums.adafruit.com/viewtopic.php?t=45405#p227425
    constexpr uint8_t mask_d = 0b01011110;
    constexpr uint8_t mask_i = 0b00000100;
    constexpr uint8_t mask_n = 0b01010100;
    constexpr uint8_t mask_g = 0b01101111;

    display.writeDigitRaw(0, mask_d);
    display.writeDigitRaw(1, mask_i);
    display.drawColon(false);
    display.writeDigitRaw(3, mask_n);
    display.writeDigitRaw(4, mask_g);
}

}  // namespace

void setup() {
    pinMode(kPinButtonSet,          INPUT_PULLUP);
    pinMode(kPinButtonPauseResume,  INPUT_PULLUP);
    pinMode(kPinButtonUp,           INPUT_PULLUP);
    pinMode(kPinButtonDown,         INPUT_PULLUP);

    pinMode(kPinSolenoidControl,    OUTPUT);

    display.begin(kDisplayI2CAddr);
    display.setBrightness(8);
}

void loop() {
    const auto cur_ts_msec = millis();
    for (size_t i = 0; i < static_length(ordered_switch_states); ++i) {
        ordered_switch_states[i]->logRead(digitalRead(kOrderedButtonPins[i]),
                                          cur_ts_msec);
    }
    const ButtonInput input = getOnlyInput();
    sm.step(input, cur_ts_msec);
    if (sm.getState() == BTS::DING) {
        writeDing();
    } else {
        if (sm.minsVisible()) {
            writeMins(sm.getMins());
        } else {
            writeMinsBlank();
        }
        if (sm.secsVisible()) {
            writeSecs(sm.getSecs());
        } else {
            writeSecsBlank();
        }
        display.drawColon(true);
    }
    display.writeDisplay();

    if (sm.strikeStartsNow()) {
        solenoid_push_expiry_ts = cur_ts_msec + kStrikeDurationMsec;
    }
    if (cur_ts_msec < solenoid_push_expiry_ts) {
        digitalWrite(kPinSolenoidControl, HIGH);
    } else {
        digitalWrite(kPinSolenoidControl, LOW);
    }
}
