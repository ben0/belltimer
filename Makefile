##### BEGIN CONFIGURABLE OPTIONS #####

# Pin numbers
PIN_BUTTON_SET           = 5
PIN_BUTTON_PAUSE_RESUME  = 4
PIN_BUTTON_UP            = 3
PIN_BUTTON_DOWN          = 2
PIN_SOLENOID_CONTROL     = 6

# I2C address of the display
DISPLAY_I2C_ADDR = 0x70

# Number of milliseconds to turn on the solenoid for each strike
STRIKE_DURATION_MSEC = 20

# Debounce delay for the button switches
BUTTON_DEBOUNCE_MSEC = 50

##### END CONFIGURABLE OPTIONS #####


ARDMK_DIR    = Arduino-Makefile
BOARD_TAG    = uno
ARDUINO_LIBS = Wire SPI Adafruit_BusIO Adafruit_GFX Adafruit_LED_Backpack

include ${ARDMK_DIR}/Arduino.mk

CXXFLAGS += -DPIN_BUTTON_SET=${PIN_BUTTON_SET}
CXXFLAGS += -DPIN_BUTTON_PAUSE_RESUME=${PIN_BUTTON_PAUSE_RESUME}
CXXFLAGS += -DPIN_BUTTON_UP=${PIN_BUTTON_UP}
CXXFLAGS += -DPIN_BUTTON_DOWN=${PIN_BUTTON_DOWN}
CXXFLAGS += -DPIN_SOLENOID_CONTROL=${PIN_SOLENOID_CONTROL}
CXXFLAGS += -DDISPLAY_I2C_ADDR=${DISPLAY_I2C_ADDR}
CXXFLAGS += -DSTRIKE_DURATION_MSEC=${STRIKE_DURATION_MSEC}
CXXFLAGS += -DBUTTON_DEBOUNCE_MSEC=${BUTTON_DEBOUNCE_MSEC}

# At time of writing, C++14 is just used for a static assertion to make sure
# the same pin number isn't used for two different things.  The check could
# maybe be rewritten in C++11, with some effort; but I don't currently see any
# reason not to just use C++14.
CXXFLAGS += -std=gnu++14
